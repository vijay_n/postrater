class Postrates < ActiveRecord::Base
  attr_accessible  :post, :created_at
  validates :post, presence: true, length: { maximum: 160 }
end
